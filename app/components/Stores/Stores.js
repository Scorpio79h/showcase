import React from "react";
import BaseLayout from "./../../base/layouts/BaseLayout";
import {Container} from "semantic-ui-react";

export default class Stores extends BaseLayout {
    render() {
        return (
            <div>
                <Container>
                    Страница магазинов
                </Container>
            </div>
        );
    }
}

import React from "react";
import BaseLayout from "./../../base/layouts/BaseLayout";
import {Container} from "semantic-ui-react";

export default class Profile extends BaseLayout {
    render() {
        return (
            <div>
                <Container>
                    Страница профиля
                </Container>
            </div>
        );
    }
}

import React from "react";
import BaseLayout from "./../../base/layouts/BaseLayout";
import {Container} from "semantic-ui-react";

export default class Services extends BaseLayout {
    render() {
        return (
            <div>
                <Container>
                    Страница услуг
                </Container>
            </div>
        );
    }
}

import React from "react";
import BaseLayout from "./../../base/layouts/BaseLayout";
import {Container} from "semantic-ui-react";

export default class Products extends BaseLayout {
    render() {
        return (
            <div>
                <Container>
                    Страница товаров
                </Container>
            </div>
        );
    }
}

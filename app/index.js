import React from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";

import store from "./store";

import BaseRoutes from "./base/BaseRoutes";

ReactDOM.render((
    <Provider store={store}>
        <BaseRoutes />
    </Provider>
), document.getElementById("showcase"));

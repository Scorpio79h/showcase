import React, {Component} from "react";

import {Link} from "react-router";
import {Container, Menu} from "semantic-ui-react";

export default class HeaderNavbar extends Component {
    render() {
        return (
            <div>
                <Container>
                    <Menu stackable>
                        <Menu.Item>
                            <Link to='/'>
                                <img src='/logo.png'/>
                            </Link>
                        </Menu.Item>
                        <Menu.Item><Link to='/'>Главная</Link></Menu.Item>
                        <Menu.Item><Link to='/profile'>Профиль</Link></Menu.Item>
                        <Menu.Item><Link to='/login'>Регистрация/Вход</Link></Menu.Item>
                    </Menu>
                    <Menu stackable>
                        <Menu.Item><Link to='/services'>Услуги</Link></Menu.Item>
                        <Menu.Item><Link to='/products'>Товары</Link></Menu.Item>
                        <Menu.Item><Link to='/stores'>Магазины</Link></Menu.Item>
                        <Menu.Item><Link to='/cities'>Города</Link></Menu.Item>
                        <Menu.Item><Link to='/promotions'>Акции</Link></Menu.Item>
                    </Menu>
                </Container>
            </div>
        );
    }
}


import React from "react";

import Profile from "./../components/Profile/Profile";
import Services from "./../components/Services/Services";
import Products from "./../components/Products/Products";
import Stores from "./../components/Stores/Stores";
import Cities from "./../components/Cities/Cities";
import Promotions from "./../components/Promotions/Promotions";

export default class ShowcaseRoutes {
    /**
     *
     * @returns {*[]}
     */
    static routes() {
        return [

            {name: "profile", path: "/profile", component: Profile},
            {name: "services", path: "/services", component: Services},
            {name: "products", path: "/products", component: Products},
            {name: "stores", path: "/stores", component: Stores},
            {name: "cities", path: "/cities", component: Cities},
            {name: "promotions", path: "/promotions", component: Promotions},

        ];
    }
};

import React from "react";
import {browserHistory, IndexRoute, Route, Router} from "react-router";
import BaseLayout from "./../base/layouts/BaseLayout";
import ShowcaseRoutes from "./ShowcaseRoutes";

import IndexShowcase from "./../base/IndexShowcase";
import Login from "./../base/layouts/LoginLayout";
import BaseNotFound from "./../base/NotFound";

export default class BaseRoutes extends React.Component {
    /**
     *
     * @returns {XML}
     */
    render() {

        var routes = ShowcaseRoutes.routes();

        return (
            <Router history={browserHistory}>
                <Route path="/" component={ BaseLayout }>
                    <IndexRoute name="index" component={IndexShowcase}/>
                    {routes.map(function (item, i) {
                        return (<Route key={i} name={item.name} path={item.path} component={item.component}/>);
                    })}
                </Route>
                <Route path='/login' component={Login}/>
                <Route path='*' component={BaseNotFound}/>
            </Router>
        );

    }
}
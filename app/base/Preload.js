import React from 'react';

export default class BasePreload extends React.Component
{
    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
			handleWatchVisible: true,
        };
    }
	
    /**
     *
     * @returns {XML}
     */
    render()
    {
        return (
            <div id="floatingBarsG" className={(!this.state.visible) ? 'hide' : ''}>
	           <div className="blockG" id="rotateG_01"></div>
	           <div className="blockG" id="rotateG_02"></div>
	           <div className="blockG" id="rotateG_03"></div>
	           <div className="blockG" id="rotateG_04"></div>
	           <div className="blockG" id="rotateG_05"></div>
	           <div className="blockG" id="rotateG_06"></div>
	           <div className="blockG" id="rotateG_07"></div>
	           <div className="blockG" id="rotateG_08"></div>
            </div>
        );
    }
};
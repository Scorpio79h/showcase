import React, {Component} from "react";
import HeaderNavbar from "./../../base/includes/HeaderNavbar";

export default class BaseLayout extends Component {

    /**
     *
     * @returns {XML}
     */
    render() {

        return (
            <div className="base-app-layout">
                <HeaderNavbar/>
                {this.props.children}
            </div>
        );

    }
};

var gulp = require('gulp'),
    browserify = require('browserify'),
    watchify = require('watchify'),
    source = require('vinyl-source-stream'),
    babelify = require('babelify');

var sourceFile = '/app/index.js',
    destFolder = './public',
    destFile = './js/app.js';

/* browserify errors */
var gutil = require('gulp-util'),
    chalk = require('chalk');

gulp.task('default', ['browserify', 'watch']);

gulp.task('browserify', function () {

    return browserify({
        entries: [
            __dirname + sourceFile
        ],
        extensions: [".js"],
        transform: [babelify],
        debug: true,
        cache: {},
        packageCache: {},
        fullPaths: true
    })
        .bundle()
        .on("error", map_error)
        .pipe(source(destFile))
        .pipe(gulp.dest(destFolder));
});

gulp.task('watch', function () {

    var bundler = watchify(browserify({
        entries: [
            __dirname + sourceFile
        ],
        extensions: [".js"],
        transform: [babelify],
        debug: true,
        cache: {},
        packageCache: {},
        fullPaths: true
    }));

    bundler.on('update', rebundle);

    function rebundle() {

        gutil.log(chalk.green("Updated JavaScript sources"));

        return bundler.bundle()
            .on("error", map_error)
            .pipe(source(destFile))
            .pipe(gulp.dest(destFolder));
    }

    return rebundle();
});

function map_error(err) {
    if (err.fileName) {
        // regular error
        gutil.log(chalk.red(err.name)
            + ': '
            + chalk.yellow(err.fileName.replace(__dirname + '/src/js/', ''))
            + ': '
            + 'Line '
            + chalk.magenta(err.lineNumber)
            + ' & '
            + 'Column '
            + chalk.magenta(err.columnNumber || err.column)
            + ': '
            + chalk.blue(err.description))
    } else {
        // browserify error..
        gutil.log(chalk.red(err.name)
            + ': '
            + chalk.yellow(err.message))
    }

    this.emit('end');
}